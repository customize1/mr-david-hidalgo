import 'package:flutter/material.dart';

class HomePage extends StatelessWidget{
  final user;
  const HomePage({super.key, required this.user});
  @override
  Widget build(BuildContext) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Welcome " + this.user),
      ),
    );
  }
}