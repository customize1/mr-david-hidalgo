import 'package:app_chat/main.dart';
import 'package:flutter/material.dart';
import 'package:app_chat/screens/login/forgot.dart';
import 'package:app_chat/home.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:email_validator/email_validator.dart';
import 'package:app_chat/utils/utils.dart';
import 'dart:io';
import './loginController.dart';

class LoginPage extends StatefulWidget{
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final formKey = GlobalKey<FormState>();
  bool log_in = true;
  final loginController = LoginController();
  Future signIn() async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => Center(child: CircularProgressIndicator())
    );

    try{
      String email = user.text.trim();
      String password = pass.text.trim();
      // final responseApi = await loginController.checkUserExisted(email);
      // if(responseApi != null){
      //   if(responseApi.success!){
          print(email);
          List<String> signInMethods = await FirebaseAuth.instance.fetchSignInMethodsForEmail(email);
          await FirebaseAuth.instance.signInWithEmailAndPassword(email: email, password: password);
          // if (signInMethods.isNotEmpty) {
          //
          // } else {
          //   // await FirebaseAuth.instance.createUserWithEmailAndPassword(email:email, password: password);
          //   Utils.showSnackBar("Not register in Wanerby App");
          // }
      //   }else{
      //     Utils.showSnackBar("Not register in Wanerby");
      //   }
      // }
    }on FirebaseAuthException catch (e) {
      Utils.showSnackBar("Not register in Wanerby App");
    }
    navigatorKey.currentState!.popUntil((route) => route.isFirst);
    // navigatorKey
  }
  Future signUp() async {
    final isValid = formKey.currentState!.validate();
    if(!isValid) return;
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => Center(child: CircularProgressIndicator())
    );
    try{
      String email = user.text.trim();
      String password = pass.text.trim();
      final responseApi = await loginController.checkUserExisted(email);

      print(responseApi);
      print('responseApi');
      if(responseApi != null){
        if(responseApi.success!){
          await FirebaseAuth.instance.createUserWithEmailAndPassword(email: email, password: password);
        }else{
          Utils.showSnackBar('Not register in Wanerby');
        }
      }
    }on FirebaseAuthException catch (e) {
      Utils.showSnackBar(e.message!);
    }
    navigatorKey.currentState!.popUntil((route) => route.isFirst);
    // navigatorKey
  }
  bool type_pass = true;
  var userNameError = "Username is wrong";
  bool userInvalid = true;
  var passwordError = "Password is wrong";
  bool passInvalid = true;
  TextEditingController user = new TextEditingController();
  TextEditingController pass = new TextEditingController();
  TextEditingController repass = new TextEditingController();

  void changeTypePassField(){
    setState(() {
      type_pass = !type_pass;
    });
  }
  void toggleSignInUp(){
    setState(() {
      log_in = !log_in;
    });
  }
  void getPageForgot(){
    Navigator.push(context, MaterialPageRoute(builder: (context) => Forgot()));
  }
  String? _validateInput(String? value) {
    if (value == null || value.isEmpty) {
      return 'Enter a email';
    }
    if(!EmailValidator.validate(value)){
      return 'Enter a valid email';
    }
    return null; // Hợp lệ
  }
  String? _validatePass(String? value) {
    if(log_in) return null;
    if (value == null || value.length < 6) {
      return 'Enter min 6 character';
    }
    return null; // Hợp lệ
  }
  @override
  Widget build(BuildContext context) {
    final ButtonStyle flatButtonStyle = TextButton.styleFrom(
      backgroundColor: Colors.blue,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
    );
    return Container(
          alignment: Alignment.bottomCenter,
          padding: EdgeInsets.fromLTRB(30,0,30,0),
          constraints: BoxConstraints.expand(),
          child: SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(
                minHeight: 0,
              ),
              child: Form(
                key: formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0,0,0,40),
                      child: Container(
                          width: 70,
                          height: 70,

                          child: Image.asset('assets/logo.jpeg')
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0 , 0 , 0 , 60),
                      child: Text("Hello \nWelcome Back", style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 30
                      ),),
                    ),
                    Padding(
                        padding: const EdgeInsets.fromLTRB( 0 , 0 ,0 ,40),
                        child: TextFormField(
                          controller: user,
                          style: TextStyle(
                              fontSize: 20,
                              color: Colors.white
                          ),
                          cursorColor: Colors.white,
                          textInputAction: TextInputAction.next,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          validator: _validateInput,
                          decoration: InputDecoration(
                              labelText: 'Email',
                              labelStyle: TextStyle(
                                  color: Color(0xff888888),
                                  fontSize: 20
                              )
                          ),
                        )
                      // TextField(
                      //   controller: user,
                      //   style: TextStyle(
                      //       fontSize: 18,
                      //       color: Colors.black
                      //   ),
                      //   decoration: InputDecoration(
                      //       errorText: !userInvalid ? userNameError : null,
                      //       labelText: 'USERNAME',
                      //       labelStyle: TextStyle(
                      //           color: Color(0xff888888),
                      //           fontSize: 15
                      //       )
                      //   ),
                      // ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB( 0 , 0 ,0 ,40),
                      child: Stack(
                          alignment: AlignmentDirectional.centerEnd,
                          children: <Widget>[
                            TextFormField(
                              controller: pass,
                              style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.white
                              ),
                              cursorColor: Colors.white,
                              obscureText: type_pass,
                              autovalidateMode: AutovalidateMode.onUserInteraction,
                              validator: _validatePass,
                              decoration: InputDecoration(
                                  labelText: 'Password',
                                  // errorText: !passInvalid ? passwordError : null,
                                  labelStyle: TextStyle(
                                      color: Color(0xff888888),
                                      fontSize: 20
                                  )
                              ),
                            ),
                            Padding(
                                padding: const EdgeInsets.fromLTRB(0 ,12,0,0),
                                child: TextButton(
                                  onPressed: changeTypePassField,
                                  child: Text(
                                    type_pass ? "SHOW" : "HIDE",
                                    style: TextStyle(
                                        color: Colors.blue,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold
                                    ) ,
                                  ),
                                )
                            )
                          ]
                      ),
                    ),
                    // if(!log_in) Padding(
                    //   padding: const EdgeInsets.fromLTRB( 0 , 0 ,0 ,40),
                    //   child: Stack(
                    //       alignment: AlignmentDirectional.centerEnd,
                    //       children: <Widget>[
                    //         TextField(
                    //           controller: repass,
                    //           style: TextStyle(
                    //               fontSize: 18,
                    //               color: Colors.black
                    //           ),
                    //           obscureText: type_pass,
                    //           decoration: InputDecoration(
                    //               labelText: 'REPASSWORD',
                    //               errorText: null,
                    //               labelStyle: TextStyle(
                    //                   color: Color(0xff888888),
                    //                   fontSize: 15
                    //               )
                    //           ),
                    //         ),
                    //         Padding(
                    //             padding: const EdgeInsets.fromLTRB(0 ,12,0,0),
                    //             child: TextButton(
                    //               onPressed: changeTypePassField,
                    //               child: Text(
                    //                 type_pass ? "SHOW" : "HIDE",
                    //                 style: TextStyle(
                    //                     color: Colors.blue,
                    //                     fontSize: 16,
                    //                     fontWeight: FontWeight.bold
                    //                 ) ,
                    //               ),
                    //             )
                    //         )
                    //       ]
                    //   ),
                    // ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0,0,0,0),
                      child: SizedBox(
                        width: double.infinity,
                        height: 56,
                        child: TextButton(
                          onPressed: log_in ? signIn : signUp,
                          style: flatButtonStyle,
                          child: Text(log_in ? "SIGN IN" : "REGISTER",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: 100,
                      width: double.infinity,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            children: [
                              Text(
                                "NEW USER? ",
                                style: TextStyle(
                                    fontSize: 15,
                                    color: Colors.white
                                ),
                              ),
                              GestureDetector(
                                onTap: toggleSignInUp,
                                child: Text(
                                  log_in ? "SIGN UP" : "SIGNIN",
                                  style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.blue
                                  ),
                                ),
                              ),
                            ],
                          ),
                          GestureDetector(
                            onTap: getPageForgot,
                            child: Text("FORGOT PASSWORD" ,
                              style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.blue
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            )
          )
      );
  }
}