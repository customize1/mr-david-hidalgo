import 'package:flutter/material.dart';
import '../../providers/webhook_provider.dart';
import '../../models/reponse_user.dart';
class LoginController{

  final WebHookProvider webHookProvider = WebHookProvider();

  Future<ResponseUser?> checkUserExisted(email) async {
    var responseApi = await webHookProvider.getUser(email);
    if (responseApi != null) {
      return responseApi;
    }else{
      return null;
    }
  }
}