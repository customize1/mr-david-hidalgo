import 'package:flutter/material.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import '../../utils/utils.dart';

class Forgot extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    final ButtonStyle flatButtonStyle = TextButton.styleFrom(
      backgroundColor: Colors.blue,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
    );
    TextEditingController email = new TextEditingController();
    String? _validateEmail(String? value) {
      if (value == null || value.isEmpty) {
        return 'Enter a email';
      }
      if(!EmailValidator.validate(value)){
        return 'Enter a valid email';
      }
      return null; // Hợp lệ
    }
    Future forgotPass() async {
      try{
        await FirebaseAuth.instance.sendPasswordResetEmail(email: email.text.trim());
        Utils.showSnackBar("Password Reset Email send");
        Navigator.of(context).popUntil((route) => route.isFirst);
      } on FirebaseAuthException catch (e){
        Utils.showSnackBar(e.message);
        Navigator.of(context).pop();
      }
    }
    return Scaffold(
      appBar: AppBar(
        title: Text("Forgot password"),
      ),
      body: Container(
        alignment: Alignment.center,
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
           child: ConstrainedBox(
             constraints: BoxConstraints(
               minHeight: 0,
             ),
             child: Padding(
               padding: const EdgeInsets.fromLTRB( 20 , 0 , 20 , 0),
               child: Column(
                 mainAxisAlignment: MainAxisAlignment.end,
                 crossAxisAlignment: CrossAxisAlignment.start,
                 children: <Widget>[
                   Padding(
                     padding: const EdgeInsets.fromLTRB( 0 , 0 , 0 ,30),
                     child: TextFormField(
                       controller: email,
                       cursorColor: Colors.white,
                       textInputAction: TextInputAction.done,
                       style: TextStyle(
                           fontSize: 20
                       ),
                       validator: _validateEmail,
                       decoration: InputDecoration(
                           labelText: 'Email',
                           labelStyle: TextStyle(
                               color: Colors.white,
                               fontSize: 20
                           )
                       ),
                     ),
                   ),
                   Padding(
                     padding: const EdgeInsets.fromLTRB(0,0,0,0),
                     child: SizedBox(
                       width: double.infinity,
                       height: 56,
                       child: ElevatedButton.icon(
                         onPressed: forgotPass,
                         style: ElevatedButton.styleFrom(
                           minimumSize: Size.fromHeight(50),
                           backgroundColor: Colors.blue,
                           shape: RoundedRectangleBorder(
                             borderRadius: BorderRadius.circular(10), // Đặt bán kính bo tròn
                           ),
                         ),
                         icon: Icon(
                           Icons.email_outlined,
                           color: Colors.white,
                         ),
                         label: Text(
                           'Reset Password',
                           style: TextStyle(
                             fontSize: 20,
                             color: Colors.white
                           ),
                         ),
                       ),
                     ),
                   ),
                 ]
               ),
             ),
           ),
        )
      ),
    );
  }
}