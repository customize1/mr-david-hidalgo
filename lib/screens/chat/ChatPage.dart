import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';

class ChatPage extends StatefulWidget{
  @override
  ChatPageState createState() => ChatPageState();
}
class ChatPageState extends State<ChatPage> {
  @override
  Widget build(BuildContext context) {
    final user = FirebaseAuth.instance.currentUser!;

    return Padding(
      padding: EdgeInsets.all(0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('Chat Page' + user.email!),
          SizedBox(height: 8,),
          ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              minimumSize: Size.fromHeight(50)
            ),
            icon: Icon(Icons.arrow_back , size: 32,),
            onPressed: () {
              FirebaseAuth.instance.signOut();
            },
            label: Text(
              'Sign out',
              style: TextStyle(fontSize: 24),
            )
          )
        ],
      )
    );
  }
}