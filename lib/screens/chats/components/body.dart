import 'package:app_chat/components/filled_outline_button.dart';
import 'package:app_chat/constants.dart';
import 'package:app_chat/models/Chat.dart';
import 'package:app_chat/screens/messages/message_screen.dart';
import 'package:flutter/material.dart';
import 'package:app_chat/providers/webhook_provider.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'chat_card.dart';

class Body extends StatelessWidget {
  final WebHookProvider _provider = WebHookProvider();
  Body({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.fromLTRB(
              kDefaultPadding, 0, kDefaultPadding, kDefaultPadding),
          color: kPrimaryColor,
          child: Row(
            children: [
              FillOutlineButton(press: () {}, text: "Recent Message"),
              const SizedBox(width: kDefaultPadding),
              FillOutlineButton(
                press: () {
                  FirebaseAuth.instance.signOut();
                },
                text: "Logout",
                isFilled: false,
              ),
            ],
          ),
        ),
        Expanded(
          // child: ListView.builder(
          //   itemCount: chatsData.length,
          //   itemBuilder: (context, index) => ChatCard(
          //     chat: chatsData[index],
          //     press: () => Navigator.push(
          //       context,
          //       MaterialPageRoute(
          //         builder: (context) => const MessagesScreen(),
          //       ),
          //     ),
          //   ),
          // ),
          child: StreamBuilder<List<Chat>>(
            stream: _provider.getUsersMessenger,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(child: CircularProgressIndicator());
              }

              print(snapshot);

              if (snapshot.hasError) {
                return Center(child: Text('Error: ${snapshot.error}'));
              }

              if (!snapshot.hasData || snapshot.data!.isEmpty) {
                return Center(child: Text('No messages'));
              }

              final messages = snapshot.data!;

              return ListView.builder(
                itemCount: messages.length,
                  itemBuilder: (context, index) => ChatCard(
                    chat: messages[index],
                    press: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const MessagesScreen(),
                      ),
                    ),
                  ),
              );
            },
          ),
        ),
      ],
    );
  }
}
