import 'dart:async';
import 'package:firebase_database/firebase_database.dart';
import '../models/Chat.dart';
import '../models/ChatMessage.dart';


class ChatProvider {
  final DatabaseReference _messagesRef = FirebaseDatabase.instance.reference().child('messages');
  final DatabaseReference _userRef = FirebaseDatabase.instance.reference().child('users');

  final StreamController<List<Chat>> _chatController = StreamController<List<Chat>>.broadcast();

  Stream<List<Chat>> get chatStream => _chatController.stream;

  ChatProvider() {
    _messagesRef.onValue.listen((event) {
      DataSnapshot snapshot = event.snapshot;
      List<ChatMessage> messages = _parseMessages(snapshot);
      _updateChats(messages);
    });

    _userRef.onValue.listen((event) {
      print('UserRef updated');
      List<Chat> chats = _parseChats(event.snapshot);
      _updateChats(chats);
    });
  }

  List<ChatMessage> _parseMessages(DataSnapshot snapshot) {
    List<ChatMessage> messages = [];
    if (snapshot.value != null) {
      Map<dynamic, dynamic> data = snapshot.value as Map<dynamic, dynamic>;
      data.forEach((key, value) {
        messages.add(ChatMessage.fromMap(Map<dynamic, dynamic>.from(value)));
      });
    }
    return messages;
  }

  List<Chat> _parseChats(DataSnapshot snapshot) {
    List<Chat> chats = [];
    if (snapshot.value != null) {
      final chatsData = snapshot.value as Map<dynamic, dynamic>;
      chatsData.forEach((key, value) {
        chats.add(Chat.fromMap(Map<dynamic, dynamic>.from(value)));
      });
    }
    return chats;
  }

  void _updateChats(List<dynamic> items) {
    // Gán tin nhắn mới nhất vào các chat
    List<Chat> updatedChats = items.map((item) {
      if (item is Chat) {
        // Cập nhật tin nhắn mới nhất cho từng chat
        String latestMessage = _getLatestMessageForChat(item.name);
        return Chat(
          name: item.name,
          lastMessage: latestMessage,
          image: item.image,
          time : item.time,
          isActive : false,
        );
      }
      return item as Chat;
    }).toList();

    _chatController.add(updatedChats);
  }

  String _getLatestMessageForChat(String chatId) {
    // Lấy tin nhắn mới nhất cho chatId (Ví dụ: từ _messagesRef)
    // Đây là ví dụ đơn giản, bạn cần thay đổi cách lấy tin nhắn mới nhất tùy theo cấu trúc dữ liệu của bạn
    return "Latest message text";
  }

  void dispose() {
    _chatController.close();
  }
}