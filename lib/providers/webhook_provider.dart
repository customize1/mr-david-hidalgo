import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import '../models/reponse_user.dart';
import '../models/Chat.dart';
import '../models/ChatMessage.dart';
import 'package:firebase_database/firebase_database.dart';

class WebHookProvider {
  String url = 'http://magento-1332672-4878534.cloudwaysapps.com/rest/V1/netbase';
  final DatabaseReference _userRef = FirebaseDatabase.instance.reference().child('users');
  final DatabaseReference _messagesRef = FirebaseDatabase.instance.reference().child('messages');
  final List<Chat> chats = [];
  final StreamController<List<ChatMessage>> _chatController = StreamController<List<ChatMessage>>.broadcast();
  Stream<List<ChatMessage>> get chatStream => _chatController.stream;

  Future<ResponseUser?> getUser(String email) async {
    final body = jsonEncode(<String, String>{
      'token': 'GksRjmW794z5tZUiUAhETY8k8ph42WlP3oqyHopPamF8GSDKM1jjsZuuur7kJUBW',
      'email': email
    });
    final String basicAuth = 'Basic ' + base64Encode(utf8.encode('m2w:m2wA1234'));
    final response = await http.post(
      Uri.parse('$url/getUser'),
      headers: <String, String>{
        'Authorization': basicAuth,
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: body,
    );
    final decodedResponse = jsonDecode(response.body);
    Map<String, dynamic> jsonMap = jsonDecode(decodedResponse);
    return ResponseUser.fromJson(jsonMap);
  }

  List<ChatMessage> _parseMessages(DataSnapshot snapshot) {
    List<ChatMessage> messages = [];

    if (snapshot.value != null) {
      Map<dynamic, dynamic> data = snapshot.value as Map<dynamic, dynamic>;
      data.forEach((key, value) {
        messages.add(ChatMessage.fromJson(Map<String, dynamic>.from(value)));
      });
    }
    return messages;
  }

  Stream<List<Chat>> get getUsersMessenger {
    _messagesRef.onValue.listen((event) {
      DataSnapshot snapshot = event.snapshot;
      List<ChatMessage> messages = _parseMessages(snapshot);
    });


    return _userRef.onValue.map((event) {
      final messagesData = event.snapshot.value as Map<dynamic, dynamic>;
      return messagesData.entries.map((entry) {
        print(entry);
        return Chat.fromMap(entry.value);
      }).toList();
    });
  }
}
