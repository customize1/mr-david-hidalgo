import 'dart:convert';


class ResponseUser {
  bool? success;
  int? userId;
  String? message;

  ResponseUser({
    this.success,
    this.userId,
    this.message
  });

  factory ResponseUser.fromJson( Map<String, dynamic> json) => ResponseUser(
    success: json["success"],
    userId: json["customer_id"],
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "userId": userId,
    "message": message,
  };
}
