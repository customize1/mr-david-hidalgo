import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import './screens/login/LoginPage.dart';
import './screens/chats/chats_screen.dart';
import './screens/chat/ChatPage.dart';
import './utils/utils.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}



class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      scaffoldMessengerKey: Utils.messengerKey,
      navigatorKey: navigatorKey,
      debugShowCheckedModeBanner: false,
      title: "Wanerby",
      theme: ThemeData(
        colorScheme: ColorScheme.dark(
          primary: Colors.teal,
          secondary: Colors.amber,
          surface: Colors.grey[800]!,
          background: Colors.black,
          error: Colors.red,
          onPrimary: Colors.white,
          onSecondary: Colors.black,
          onSurface: Colors.white,
          onBackground: Colors.white,
          onError: Colors.white,
        ),
      )
      ,
      home : MainPage()
    );
  }
}

final navigatorKey = GlobalKey<NavigatorState>();

class MainPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) =>
    Scaffold(
      body : StreamBuilder<User?>(
        stream: FirebaseAuth.instance.authStateChanges(),
        builder: (context, snapshot){
          if(snapshot.connectionState == ConnectionState.waiting){
            return Center(
              child: CircularProgressIndicator(),
            );
          }else if(snapshot.hasError){
            return Center(child: Text('Some thing went wrong'));
          }else if(snapshot.hasData) {
            return ChatsScreen();
          }else {
            return LoginPage();
          }
        },
      )
    );
}